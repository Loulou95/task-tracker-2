<?php

use Illuminate\Database\Seeder;
use App\Models\Project;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $projects = [
            [
                'id' => 4,
                'name' => 'Personal',
                'user_id' => 1,
            ],
            [
                'id' => 3,
                'user_id' => 1,
                'name' => 'Homework',
            ]
        ];

        foreach($projects as $project) {
            Project::updateOrCreate(
                [
                    'id'=>$project['id']
                ],
                $project
            );
        }
    }
}
