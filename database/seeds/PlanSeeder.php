<?php

use Illuminate\Database\Seeder;
use App\Models\Plan;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = [
            [
                'id' => 1,
                'name' => 'Basic',
                'price' => '£0',
                'subscription_type' => 'Monthly',
                'Description' => 'This plan is totally free. No hidden cost.',
                'active' => true
            ],
            [
                'id' => 2,
                'name' => 'Silver',
                'price' => '£6',
                'subscription_type' => 'Monthly',
                'Description' => 'This is a monthly membership, No hidden cost.',
                'active' => false
            ],
            [
                'id' => 3,
                'name' => 'Gold',
                'price' => '£45',
                'subscription_type' => 'Yearly',
                'Description' => 'This is a yearly membership. No hidden cost',
                'active' => false
            ]
        ];

        foreach($plans as $plan) {
            Plan::updateOrCreate(
                [
                    'id'=>$plan['id']
                ],
                $plan
            );
        }
    }
}
