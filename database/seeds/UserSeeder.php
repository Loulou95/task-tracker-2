<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'id' => 1,
                'email' => 'louis@tasktracker',
                'name' => 'Louis',
                'password' => Hash::make('Tester12?'),
            ],
            [
                'id' => 2,
                'email' => 'nadia@tasktracker.com',
                'name' => 'Nadia',
                'password' => Hash::make('Tester12?'),
            ]
        ];

        foreach($users as $user) {
            \App\User::updateOrCreate(
                [
                    'id'=>$user['id']
                ],
                $user
            );
        }
    }
}
