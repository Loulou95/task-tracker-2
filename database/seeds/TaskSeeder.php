<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Task;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tasks = [
            [
                'id' => 2,
                'user_id' => 1,
                'description' => 'Make sure you make',
                'Title' => 'Go to the shop',
                'status' => 'Complete',
                'priority' => 'High',
                'due_date' => Carbon::create('2021', '03', '30'),
            ],
            [
                'id' => 9,
                'user_id' => 1,
                'description' => 'wicket wicket',
                'Title' => 'Go to the shop',
                'status' => 'Complete',
                'priority' => 'High',
                'due_date' => Carbon::create('2021', '03', '30'),
            ],
            [
                'id' => 6,
                'project_id' => 4,
                'user_id' => 2,
                'description' => 'Make sure you make sure',
                'Title' => 'Go to the shop',
                'status' => 'Complete',
                'priority' => 'medium',
                'due_date' => Carbon::create('2021', '03', '29'),
            ],
            [
                'id' => 8,
                'user_id' => 2,
                'description' => 'Get this done',
                'Title' => 'Go to the shop',
                'status' => 'Complete',
                'priority' => 'medium',
                'due_date' => Carbon::create('2021', '04', '29'),
            ]
        ];

        foreach($tasks as $task) {
            Task::updateOrCreate(
                [
                    'id'=>$task['id']
                ],
                $task
            );
        }
    }
}
