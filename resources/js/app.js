require("./bootstrap");

window.Vue = require("vue");

window.axios = require("axios");


import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import '@mdi/font/css/materialdesignicons.css'
import VueRouter from "vue-router";
import { routes } from "./routes";

import Account from "./components/User/index";
import AppViewComponent from "./components/AppViewComponent";


Vue.use(Vuetify);
Vue.use(VueRouter);


const vuetify = new Vuetify({
    iconfont: 'mdi',
    theme: {
        themes: {
            light: {
                primary: "#0686d8",
                secondary: "#47c7ec",
                accent: "#FDB72D",
                navigation: '#4353FF',
            }
        }
    }
});

const router = new VueRouter({
    mode: "history",
    routes: routes
});


const app = new Vue({
    router: router,
    vuetify,
    components: {
        AppViewComponent,
        "account-details": Account,
    },
    el: "#app",
});
