
import DashboardView from "./components/Dashboard/index";
import Projects from "./components/Projects/index";
import Tasks from "./components/Tasks/index";
import NewTask from "./components/Tasks/newTask";
import Plan from "./components/Plan/index";


export const routes = [
    {
        path: "/dashboard",
        name: "dashboard",
        component: DashboardView
    },
    {
        path: "/projects",
        name: "projects",
        component: Projects
    },
    {
        path: "/tasks",
        name: "tasks",
        component: Tasks
    },
    {
        path: "/new-task",
        name: "new-task",
        component: NewTask,
    },
    {
        path: "/plan",
        name: "plan",
        component: Plan,
    }
];
