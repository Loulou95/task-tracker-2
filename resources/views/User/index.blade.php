<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div class="container">
    @include('layouts.header')
   <div class="main">
       <div id="app">
           <account-details></account-details>
       </div>
   </div>
</div>
<script src="{{ asset('/js/app.js') }}" defer></script>
</body>
</html>
