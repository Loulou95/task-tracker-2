@extends('layouts.app')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="container">
        @include('layouts.header')
        <section class="inner-section">
            <div class="theme-register-section text-center mt-5">
                <p class="text-gray-500 font-bold">Signup to task-tracker</p>
                <div class="theme-signup-form-inner mt-5">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <input type="text" placeholder="Name" class="theme-form-fields" name="name" @error('name') is-invalid @enderror">
                        @error('name')
                            <span class="error-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                        <input type="text" placeholder="Email" class="theme-form-fields mt-5" name="email">
                        @error('email')
                            <span class="error-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                        <input type="password" placeholder="Create password" class="theme-form-fields mt-5 @error('password') is-invalid @enderror" name="password" required>
                        @error('password')
                        <span class="error-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                        <input type="password" placeholder="Confirm password" class="theme-form-fields mt-5 @error('password') is-invalid @enderror" name="password_confirmation" required>
                        @error('password')
                        <span class="error-block" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                        <div class="check-box flex flex-wrap text-center p-2 mt-5">
                            <div class="theme-checkbox">
                                <input type="checkbox">
                            </div>
                            <div class="theme-checkbox-text ml-4">
                                <p class="text-xs checkbox-text">Yes! send me news and offers from task-tracker about products, events and more.</p>
                            </div>
                        </div>
                        <div class="mt-5">
                            <p class="text-xs checkbox-text">By signing up, I accept the Task Tracker Terms of Service and acknowledge the Privacy Policy.</p>
                        </div>
                        <button class="rounded-sm bg-blue-700 text-white w-full mt-5 p-2">
                            Sign up
                        </button>
                    </form>
                    <div class="mt-2">
                        <p>OR</p>
                    </div>
                    <div class="theme-social-btn-section mt-2">
                        <button class="rounded-sm text-white w-full p-2 theme-social-btn font-bold theme-google-btn">
                            Continue with Google
                        </button>
                        <button class="rounded-sm text-white w-full mt-5 p-2 theme-social-btn font-bold  bg-blue-900">
                            Continue with Facebook
                        </button>
                    </div>
                    <div class="theme-form-divider mt-5">
                    </div>

                    <div class="text-center mt-2">
                        <a href="#" class="text-xs checkbox-text text-blue-600">Already have a task-tracker account? Log in</a>
                    </div>
                </div>
            </div>
        </section>
    </div>
</body>

</html>

