@extends('layouts.app')
    <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div class="container">
    @include('layouts.header')
    <section class="inner-section">
        <div class="theme-register-section text-center mt-5">
            <p class="text-gray-500 font-bold">Log in to task-tracker</p>
            <div class="theme-signup-form-inner mt-5">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <input type="text" placeholder="Enter email" class="theme-form-fields" name="email" required>
                    @error('email')
                    <span class="error-block" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                    <input type="password" placeholder="password" class="theme-form-fields mt-5" name="password" required>
                    @error('password')
                    <span class="error-block" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                    <button class="rounded-sm bg-blue-700 text-white w-full mt-5 p-2">
                        {{ __('Login') }}
                    </button>
                </form>
                <div class="mt-2">
                    <p>OR</p>
                </div>
                <div class="theme-social-btn-section mt-2">
                    <button class="rounded-sm text-white w-full p-2 theme-social-btn font-bold theme-google-btn">
                        Continue with Google
                    </button>
                    <button class="rounded-sm text-white w-full mt-5 p-2 theme-social-btn font-bold  bg-blue-900">
                        Continue with Facebook
                    </button>
                </div>
                <div class="theme-form-divider mt-5">
                </div>

                <div class="text-center mt-2 flex flex-wrap m-auto justify-evenly mt-5">
                    <a href="#" class="text-xs checkbox-text text-blue-600">Can't Log in?</a>
                    <a href="#" class="text-xs checkbox-text text-blue-600">Sign up for an account</a>
                </div>
            </div>
        </div>
    </section>
</div>
</body>

</html>

