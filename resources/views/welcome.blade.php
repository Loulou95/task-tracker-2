<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    </head>
    <body>
    <div class="container">
        @if (Auth::check())
            <app>
                <div id="app" data-app="true">
                    <app-view-component></app-view-component
                </div>
            </app>
        @else
            @include('layouts.header')
            <div class="theme-home-text-header">
                <p class="font-bold lg:text-5xl text-3xl text-center text-gray-700 mt-10 w-9/12 m-auto">Work the way that works for you.</p>
            </div>
            <div class="theme-home-text-header">
                <p class="font-bold md:text-xl text-center text-gray-700 mt-10 w-9/12 m-auto">See why thousands of people love task-tracker.</p>
            </div>
            <div class="text-center mt-20">
                <button class="theme__get__started text-center">
                    <a class="text-xl" href="{{ route('register') }}">
                        Get started
                    </a>
                </button>
            </div>
        @endif
    </div>
    <script src="{{ asset('/js/app.js') }}" defer></script>
    </body>
</html>
