<head>
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>
<div class="main">
    <div class="header  flex flex-row justify-between p-8">
        <div class="mr-10 theme__font">
            <a href="/" class="text-2xl">
                Task-tracker
            </a>
        </div>
        <ul class="menu">
            @if (Route::has('login'))
                @auth
                     <li>
                         <a class="menuLink" href="{{ route('logout') }}">Logout</a>
                     </li>
                @else
                    <li>
                        <a class="menuLink" href="{{ route('login') }}">Log in</a>
                    </li>
                @endauth
            @endif
            <li><a class="menuLink" href="#">Get started</a></li>
            <li><a class="menuLink" href="#">Product</a></li>
            <li><a class="menuLink" href="#">Enterprise</a></li>
            <li><a class="menuLink" href="#">Pricing</a></li>
        </ul>
        <nav class="flex flex-wrap flex-grow items-center justify-end theme-desktop-menu">
            <div class="flex flex-row flex-wrap flex-grow flex-shrink-0">
                <div class="left__navigation__link flex flex-wrap">
                    <div class="product mr-10">
                        <a href="#">Product</a>
                    </div>
                    <div class="product mr-10">
                        <a href="#">Enterprise</a>
                    </div>
                    <div class="product">
                        <a href="#">Pricing</a>
                    </div>
                </div>
            </div>
            <div class="flex flex-row flex-wrap flex-grow flex-shrink-0">
                <div class="flex flex-row flex-wrap flex-grow flex-shrink-0 justify-center">
                    @if (Route::has('login'))
                        @auth
                            <div class="mr-10 text-lg">
                                <a href="{{ route('account') }}">Hi {{Auth::user()->name}}</a>
                            </div>
                            <div class="">
                                <a class="text-lg" href="{{ route('logout') }}">Logout</a>
                            </div>
                        @else
                            <div class="">
                                <a class="text-lg" href="{{ route('login') }}">Log in</a>
                            </div>
                        @endauth
                    @endif
                </div>
            </div>
            <div class="get__started">
                <div class="flex flex-wrap flex-shrink-0">
                    @if (Route::has('register'))
                        <button class="theme__get__started">
                            <a class="" href="{{ route('register') }}">
                                Get started
                            </a>
                        </button>
                    @endif
                </div>
            </div>
        </nav>
        <button class="ham theme-mobile-menu">
            <span class="menuIcon material-icons">
            menu
            </span>
            <span class="xIcon">
                X
            </span>
        </button>

        <script>
            var menu = document.querySelector(".menu")
            var ham = document.querySelector(".ham")
            var xIcon = document.querySelector(".xIcon")
            var menuIcon = document.querySelector(".menuIcon")

            ham.addEventListener("click", toggleMenu)

            function toggleMenu() {
                if (menu.classList.contains("showMenu")) {
                    menu.classList.remove("showMenu");
                    xIcon.style.display = "none";
                    menuIcon.style.display = "block";
                } else {
                    menu.classList.add("showMenu");
                    xIcon.style.display = "block";
                    menuIcon.style.display = "none";
                }
            }

            var menuLinks = document.querySelectorAll(".menuLink")

            menuLinks.forEach(
                function (menuLink) {
                    menuLink.addEventListener("click", toggleMenu)
                }
            )
        </script>
    </div>

</div>

