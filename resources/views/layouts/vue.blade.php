<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div class="container">
    <app>
        <div id="app" data-app="true">
            @if (Auth::check())
                <app-view-component></app-view-component>
            @endif
        </div>
    </app>
</div>
<script src="{{ asset('/js/app.js') }}" defer></script>
</body>
</html>
