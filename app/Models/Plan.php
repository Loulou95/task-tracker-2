<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = [
        'name',
        'price',
        'description',
        'subscription_type',
        'active',
    ];
}
