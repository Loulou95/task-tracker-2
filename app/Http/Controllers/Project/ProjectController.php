<?php

namespace App\Http\Controllers\Project;

use App\Illuminate\Routing\ResponseFactory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\Task;
use App\Models\ProjectTask;

class ProjectController extends Controller
{

    public function store(Request $request) {
        $fields = $request->all();
        $fields['user_id'] = Auth::user()->id;

        Project::create($fields);
    }

    public function projects() {
        $user = Auth::user();
        return  json_encode(
            ['projects' => $user->projects]
        );
    }

    public function addTaskToProject(Request $request) {
        $task_id = $request->task_id;
         Task::where('id',  $task_id)
            ->update(
                [
                    'project_id' => $request->project_id
                ]
            );
    }

    public function getTasks($projectId) {
        $tasks = Task::where('project_id', $projectId)
            ->get();
        return  json_encode(
            ['tasks' => $tasks]
        );
    }
}
