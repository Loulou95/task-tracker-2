<?php

namespace App\Http\Controllers\Task;

use App\Http\Controllers\Controller;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    public function store(Request $request) {
        $fields = $request->all();
        $fields['user_id'] = Auth::user()->id;

        Task::create($fields);
    }

    public function tasks() {
        $user = Auth::user();
        return  json_encode(
            [
                'tasks' => $user->tasks
            ]
        );
    }

    public function delete($id) {
        $task = Task::where('id', $id)->first();
        $task->delete();
    }

    public function update(Request $request, $id) {
        $fields = $request->all();
        $task = Task::where('id', $id)->first();
        $task->update($fields);
    }
}
