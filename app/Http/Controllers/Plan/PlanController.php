<?php

namespace App\Http\Controllers\Plan;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Plan;
use App\User;

class PlanController extends Controller
{
    public function index() {
        $plans = Plan::all();

        return json_encode(
            [
                'plans' => $plans
            ]
        );
    }

    public function switch(Request $switchedPlan) {
        $user = Auth::user();

        Plan::where('id', $user->plan_id)
            ->first()->update(
                [
                    'active' => false,
                ]
            );

        Plan::where('id', $switchedPlan->id)
            ->first()->update(
                [
                    'active' => true
                ]
            );
        User::where('plan_id', $user->plan_id)
            ->update(
                [
                    'plan_id' => $switchedPlan->id
                ]
            );

    }
}
