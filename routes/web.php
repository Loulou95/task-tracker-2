<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Project'], function () {
    Route::post('add-project', 'ProjectController@store')->name('create-project');
    Route::get('all-projects', 'ProjectController@projects')->name('all-projects');
    Route::post('add-task-to-project', 'ProjectController@addTaskToProject');
    Route::get('/get-tasks/{id}', 'ProjectController@getTasks');
});

Route::group(['namespace' => 'Task'], function () {
    Route::post('store', 'TaskController@store')->name('create-task');
    Route::delete('delete-task/{id}', 'TaskController@delete')->name('delete-task');
    Route::put('update-task/{id}', 'TaskController@update')->name('update-tasks');
    Route::get('all-tasks', 'TaskController@tasks')->name('all-tasks');
});

Route::group(['namespace' => 'Plan'], function () {
    Route::get('get-plans', 'PlanController@index')->name('get-all-plans');
    Route::post('switch-plan', 'PlanController@switch')->name('switch-plan');
});

Route::get('/', 'Controller@index');
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::post('/register', 'Auth\LoginController@register')->name('register');
Route::get('logout', 'Auth\LoginController@logout');
Auth::routes();

Route::group(['namespace' => 'User'], function () {
    Route::get('/account', 'UserController@index')->name('account');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/{vue_capture?}', function () {
    return view('layouts.vue');
})->where('vue_capture', '[\/\w\.-]*');
